package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {
	@Query("select t from Task t where t.created_date between ?1 and ?2 and t.content like %?3% and t.status = ?4")
	List<Task> findByAllData(Timestamp start, Timestamp end, String content, Integer statusInt);

	@Query("select t from Task t where t.created_date between ?1 and ?2 and t.content like %?3%")
	List<Task> findByDate(Timestamp start, Timestamp end, String content);

	@Query("select t from Task t order by t.created_date desc")
	List<Task> findByOrderCreatedDateDesc();
}