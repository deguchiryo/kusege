package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@Controller
public class TaskController {

	@Autowired
	TaskService taskService;

	// タスク表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// タスク取得
		List<Task> tasks = taskService.findAllTask();
		mav.setViewName("/top");
		List<String> statusArray = new ArrayList<>();
		statusArray.add("選択なし");
		statusArray.add("完了");
		statusArray.add("ステイ中");
		statusArray.add("実行中");
		mav.addObject("tasks", tasks);
		mav.addObject("statusArray", statusArray);

		return mav;
	}

	// タスク追加画面
	@GetMapping("/new")
	public ModelAndView newContent(@ModelAttribute("formModel") Task taskFailure) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Task task = new Task();
		mav.setViewName("/new");
		List<String> statusArray = new ArrayList<>();
		statusArray.add("完了");
		statusArray.add("ステイ中");
		statusArray.add("実行中");

		if(taskFailure == null) {
			mav.addObject("formModel", task);
		}
		mav.addObject("statusArray", statusArray);

		return mav;
	}

	// タスク追加処理
	@PostMapping("/add")
	public ModelAndView addCotent(@Validated @ModelAttribute("formModel") Task task, BindingResult result,
			@RequestParam String limit, RedirectAttributes attributes) {

		List<String> errorMessages = new ArrayList<>();
		errorMessages = taskService.isEmpty(limit);
		// バリデーションエラーになった場合の処理
		if(result.hasErrors() || errorMessages.size() > 0) {
			for(ObjectError error : result.getAllErrors()) {
				errorMessages.add(error.getDefaultMessage());
			}
			attributes.addFlashAttribute("errorMessages", errorMessages);
			attributes.addFlashAttribute("formModel", task);
			attributes.addFlashAttribute("limit_date", limit);

			return new ModelAndView("redirect:/new");
		}

		// パラメータから送られてくる値をTimestampに変換する為の処理
		String limitDateFormat = limit + ":00";
		String limitDateReplace = limitDateFormat.replace("T", " ");

		// 現在日時より前の日時が入力された時のエラー処理
		if(taskService.isValid(limitDateReplace)) {
			errorMessages.add("無効な日付です");
			attributes.addFlashAttribute("errorMessages", errorMessages);
			attributes.addFlashAttribute("formModel", task);
			attributes.addFlashAttribute("limit_date", limit);

			return new ModelAndView("redirect:/new");
		}

		Timestamp limitDate =  Timestamp.valueOf(limitDateReplace);
		// entityにlimit_dateをセット
		task.setLimit_date(limitDate);

		// タスクをテーブルに格納
		taskService.saveTask(task);

		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {

		// 投稿をテーブルから削除
		taskService.deleteTask(id);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@ModelAttribute("formModel") Task taskFailure,
			@PathVariable String id, RedirectAttributes attributes) {
		ModelAndView mav = new ModelAndView();

		// 不正なパラメータが入力された時のエラー処理
		if(id.isEmpty() || !id.matches("^[0-9]*$") || taskService.editTask(Integer.parseInt(id)) == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("不正なパラメータです");
			attributes.addFlashAttribute("errorMessages", errorMessages);

			return new ModelAndView("redirect:/");
		}

		// 編集する投稿を取得
		Task task = taskService.editTask(Integer.parseInt(id));

		if(taskFailure.getContent() == null) {
			// task.limit_dateを文字列の変換
			SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String limitDate = DateFormat.format(task.getLimit_date());
			limitDate = limitDate.replace(" ", "T");

			mav.addObject("limitDate", limitDate);
		}

		// 画面遷移先を指定
		mav.setViewName("/edit");
		// 編集する投稿をセット
		if(taskFailure.getContent() == null) {
			mav.addObject("formModel", task);
		}

		return mav;
	}

	// 不正なパラメータが入力された時の処理
	@GetMapping("/edit")
	public ModelAndView parameterError(RedirectAttributes attributes) {
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータです");
		attributes.addFlashAttribute("errorMessages", errorMessages);

		return new ModelAndView("redirect:/");
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @Validated @ModelAttribute("formModel") Task task,
			BindingResult result, @RequestParam("limit") String limit, RedirectAttributes attributes) {

		// バリデーションエラーになった場合の処理
		if(result.hasErrors()) {
			List<String> errorMessages = new ArrayList<>();
			for(ObjectError error : result.getAllErrors()) {
				errorMessages.add(error.getDefaultMessage());
			}
			attributes.addFlashAttribute("errorMessages", errorMessages);
			attributes.addFlashAttribute("formModel", task);
			attributes.addFlashAttribute("limitDate", limit);

			return new ModelAndView("redirect:/edit/{id}");
		}

		// 日時が空の場合の処理
		if(ObjectUtils.isEmpty(limit)) {
			List<String> errorMessages = new ArrayList<>();
			errorMessages.add("期限を入力してください");
			attributes.addFlashAttribute("errorMessages", errorMessages);
			attributes.addFlashAttribute("formModel", task);
			attributes.addFlashAttribute("limitDate", limit);

			return new ModelAndView("redirect:/edit/{id}");
		}


		String limitDateFormat = limit + ":00";
		String limitDateReplace = limitDateFormat.replace("T", " ");

		// 現在日時より前の日時が入力された時のエラー処理
		if(taskService.isValid(limitDateReplace)) {
			List<String> errorMessages = new ArrayList<>();
			errorMessages.add("無効な日付です");
			attributes.addFlashAttribute("errorMessages", errorMessages);
			attributes.addFlashAttribute("formModel", task);
			attributes.addFlashAttribute("limitDate", limit);

			return new ModelAndView("redirect:/edit/{id}");
		}

		Timestamp limitDate =  Timestamp.valueOf(limitDateReplace);

		task.setLimit_date(limitDate);

		// 投稿をテーブルに格納
		taskService.saveTask(task);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 絞込処理
	@GetMapping("/search")
	public ModelAndView searchContent(@RequestParam("start") String start, @RequestParam("end") String end,
			@RequestParam("content") String content, @RequestParam("status") String statusValue) {
		ModelAndView mav = new ModelAndView();
		// タスクの絞込
		List<Task> tasks = taskService.searchTask(start, end, content, statusValue);
		mav.setViewName("/top");
		// ステータスの表示
		List<String> statusArray = new ArrayList<>();
		statusArray.add("選択なし");
		statusArray.add("完了");
		statusArray.add("ステイ中");
		statusArray.add("実行中");

		mav.addObject("tasks", tasks);
		mav.addObject("statusArray", statusArray);
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.addObject("content", content);
		mav.addObject("statusValue", statusValue);

		return mav;
	}

	// ステータス変更処理
	@PutMapping("/status/{id}")
	public ModelAndView statusContent (@PathVariable Integer id, @RequestParam("status") Integer status) {

		Task task = taskService.editTask(id);

		task.setStatus(status);

		// 投稿をテーブルに格納
		taskService.saveTask(task);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
