package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class TaskService {

	@Autowired
	TaskRepository taskRepository;

	// タスク全件取得
	public List<Task> findAllTask() {
		return taskRepository. findByOrderCreatedDateDesc();
	}

	// タスク追加
	public void saveTask(Task task) {
		taskRepository.save(task);
	}

	// タスク削除
	public void deleteTask(Integer id) {
		taskRepository.deleteById(id);
	}

	// タスク1件取得
	public Task editTask(Integer id) {
		Task task = (Task) taskRepository.findById(id).orElse(null);
		return task;
	}

	// タスク絞込
	public List<Task> searchTask(String start, String end, String content, String status) {
		// 現在の時刻を取得
		Date date = new Date();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String defaultEnd = dateformat.format(date);

		if(StringUtils.hasText(start)) {
			start = start + " 00:00:00";
		} else {
			start = "2020-08-01 00:00:00";
		}

		if(StringUtils.hasText(end)) {
			end = end + " 00:00:00";
		} else {
			end = defaultEnd;
		}

		Timestamp startDate = Timestamp.valueOf(start);
		Timestamp endDate = Timestamp.valueOf(end);

		Integer statusInt = Integer.parseInt(status);

		List<Task> tasks;
		if(statusInt == 0) {
			tasks = taskRepository.findByDate(startDate, endDate, content);
		} else {
			tasks = taskRepository.findByAllData(startDate, endDate, content, statusInt);
		}

		return tasks;
	}

	// 日時が空の場合のエラー処理
	public List<String> isEmpty(String limit) {
		List<String> errorMessages = new ArrayList<>();

		if(ObjectUtils.isEmpty(limit)) {
			errorMessages.add("期限を入力してください");
		}

		return errorMessages;
	}

	// 現在日時より前の日時が入力されたかの比較処理
	public boolean isValid(String limit) {
		Date date = null;
		try {
			String limitDateReplace = limit.replace("-", "/");
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	        date = sdFormat.parse(limitDateReplace);
		} catch (ParseException e) {
            e.printStackTrace();
        }

		Date dateNow = new Date();

		if(date.before(dateNow)) {
			return true;
		}

		return false;
	}
}
