$(function() {
	let date = $(".js").text();
	let date1 = date.split(" ")

	// 現在の日付を取得
	let today = new Date();
	let year = today.getFullYear();
    let month = today.getMonth()+1;
    let day = today.getDate();
    let nowDateFormat = year + "-0" + month + "-" + day;

    date1.forEach(function(value, index) {
    	if(nowDateFormat <= value) {
    		$("#" + index).addClass('date-false');
    	} else {
    		$("#" + index).addClass('date-true');
    	}
	});
});